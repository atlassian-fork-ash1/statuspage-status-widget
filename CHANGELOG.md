# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.4] - 2019-07-02
### Changed
- Updated downstream dependencies
- Fixed package.json to not use strict `yarn` and `node` versions

## [1.0.3] - 2018-12-05
### Fixed
- Fix missing static indicator colors on basic widget

## [1.0.2] - 2018-12-03
### Changed
- Add babel step to fix rendering on IE11 (#1) [@ayushya](https://bitbucket.org/ayushya/)

## [1.0.1] - 2018-11-16
### Changed
- Initial release
