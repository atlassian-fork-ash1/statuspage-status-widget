# status-widget

A [Skate](https://github.com/skatejs/skatejs)-based web component for showing an embeddable status.

## Usage

Include the [webcomponents.js](http://webcomponents.org/polyfills/) polyfill loader (for browsers who don't natively support web components), then import `status-widget`:

```html
<head>
  <!-- Required for IE11 Only -->
  <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>

  <!-- Standard dependencies -->
  <script src="https://unpkg.com/@webcomponents/webcomponentsjs@2.1.3/webcomponents-bundle.js"></script>
  <script src="https://unpkg.com/@statuspage/status-widget/dist/index.js"></script>
</head>
```

Then add the `statuspage-widget` to your page, providing a `src` attribute to the Statuspage you want to show the status for.

```html
<statuspage-widget src="https://metastatuspage.com"></statuspage-widget>
```

*** Properties ***

* `src` (required): url of Statuspage to be queried
* `appearance` (optional): the template to select for rendering (default: 'basic')
* `title` (optional): used by some templates as a heading or display text (default: 'system status')

## Developing, Testing, and Contributing

After cloning the repository, run `yarn` to install dependencies.

```shell
yarn
```

To start a local web server which hosts the demo page and assets:

```shell
yarn start
```

To run unit tests locally:

```shell
yarn test
```
