export interface Summary {
  page: PageCommons;
  components: Component[];
  incidents: Incident[];
  scheduled_maintenances: Incident[];
  status: StatusCommons;
}

export interface PageCommons {
  id: string;
  name: string;
  url: string;
  time_zone: string;
  updated_at: Date;
}

export interface StatusCommons {
  indicator: string;
  description: string;
}

export interface Component {
  status: ComponentStatus;
  name: string;
  created_at: Date;
  updated_at: Date;
  position: number;
  description: string | null;
  showcase: boolean;
  id: string;
  group_id: string | null;
  page_id: string;
  group: boolean;
}

export interface Incident {}

export enum ComponentStatus {
  OPERATIONAL = 'operational',
  UNDER_MAINTENANCE = 'under_maintenance',
  DEGRADED_PERFORMANCE = 'degraded_performance',
  PARTIAL_OUTAGE = 'partial_outage',
  MAJOR_OUTAGE = 'major_outage',
}
