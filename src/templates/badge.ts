import { html } from 'lit-html';
import { Summary } from '../types/summary';
import { Props } from '..';

export default function template(summary: Summary, props: Props) {
  return html`
    <style>
      :host {
        display: inline-block;
      }

      a.badge,
      a.badge:visited,
      a.badge:hover {
        text-decoration: none;
        color: #fff;
      }

      .status-container.badge {
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto',
          'Oxygen', 'Ubuntu', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
          sans-serif;
        font-size: 0.75rem;
        line-height: 1.25rem;
        color: #fff;
        display: flex;
        align-items: center;
        height: 1.5rem;
        padding: 0.25rem;
      }

      .badge .status-indicator {
        background-color: #aaa;
        padding: 0.25rem 0.5rem;
        line-height: 1rem;
        border-radius: 0 4px 4px 0;
        text-transform: lowercase;
      }

      .badge .none {
        background-color: #2ecc71;
      }
      .badge .operational {
        background-color: #2ecc71;
      }
      .badge .minor {
        background-color: #f1c40f;
      }
      .badge .major {
        background-color: #e67e22;
      }
      .badge .critical {
        background-color: #e74c3c;
      }
      .badge .maintenance {
        background-color: #3498db;
      }

      .badge .status-text {
        line-height: 1rem;
        border-radius: 4px 0 0 4px;
        color: #fff;
        background-color: #5a5a5a;
        padding: 0.25rem 0.5rem;
      }
    </style>

    <a class="badge" href="${props.src}" target="_blank">
      <div class="status-container badge">
        <div class="status-text">${props.title || 'system status'}</div>
        <div class="status-indicator ${summary.status.indicator}">
          ${summary.status.description}
        </div>
      </div>
    </a>
  `;
}
